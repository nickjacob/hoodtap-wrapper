require 'hoodtap/common'
require 'hoodtap/config'
require 'hoodtap/resource'
require 'hoodtap/spec'

module HoodTap

  configure do
    url ENV['HOODTAP_URL'] || API_SPEC[:url]
    page 10
    api_key ''

    on_error do |err|
      throw err
    end
  end

  begin
    API_SPEC[:resources].each do |resource, opt|
      opt = opt.is_a?(Hash) ? opt : {}
      HoodTap.const_set resource, Class.new(HoodTap::Resource) do |r|
        r.const_set "Resource", "#{resource.downcase}s"
        r.const_set "Methods", opt['methods'].map(&:to_sym) if opt.include? 'methods'
        r.const_set "Actions", opt['actions'] || {}
      end
    end
  rescue StandardError => e
    throw HoodTap::Error.new e.message
  end
end
