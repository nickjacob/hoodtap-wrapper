module HoodTap

  API_SPEC = {

    :resources => {
      :Venue => nil,
      :User => {
        :actions => {
          :login => {
            :method => 'post'
          }
        }
      }
    },

    :url => ''

  }
end
