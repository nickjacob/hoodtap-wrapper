module HoodTap

  class Resource
    Methods = [
      :post,
      :get,
      :put,
      :delete,
    ]

    def initialize data={}
      @_id = data['id']
      @_data = data
    end

    def self.method_missing(method, *args, &block)
      method = method.to_s
      opt = self.collect_query args, &block

      if method =~ /^find_by_(.+)$/
        opt[$1] = (args and args.length == 1) ? args[0] : args
        self._where opt
      elsif self::Actions.include? method
        arg = (self::Actions[method]['defaults'] || {}).merge(opt).merge((args && args.length > 0) ? args[0] : {})
        self.create_instances(self.call_api(self::Actions[method]['method'].to_sym, method, arg))
      end
    end


    def self.where qry, &block
      self._where(self.collect_query qry, &block)
    end

    def self.find id=nil
      id.nil? ? self._where : self._by_id(id)
    end

    def self.all
      self._where
    end

    def self.update id, opt
      self.call_api :put, id, opt
    end

    def self.delete id, opt={}
      self.create_instances(self.call_api :delete, id, opt)
    end

    def self.create opt={}, &block
      self.create_instances(self.call_api :post, '', self.create_query(opt, &block))
    end

    def method_missing(method, *args, &block)
      method = method.to_s
      special = nil

      if method[-1] =~ /(=|\?|!)/
        special = $1
        method = method[0..-1]
      end

      if @_data.include? method
        case special
        when nil
          @_data[method]
        when "="
          @_data[method] = args and args[0]
        when ("!" and not args.nil?)
          @_data[method] = args[0] if args[0].class == @_data[method].class
        end
      elsif special.eql? '?'
        false
      end

    end

    def delete
      self.class.call_api :delete, @_id
    end

    def save
      self.class.call_api :put, @_id, @_data
    end

    def save!
      self.class.call_api :put, @_id, @_data
    end

    def update opt
      self.class.call_api :put, @_id, opt
    end

    def safe_update values
      values.each do |k,v|
        if @_data.keys.include? k
          @_data[k] = v
        end
      end
      self.save!
    end

    def to_json
      @_data.to_json
    end

    def raw
      @_data
    end


    private

    def self.create_instances result
      if result.is_a?(Array) and result[0].include?('id')
        result.map do |r|
          self.new r
        end
      elsif result.is_a?(Hash) and result.include?('id')
        self.new result
      else
        result
      end
    end

    def self.collect_query args, &block
      args = (args && args.length > 0) ? args[0] : args
      (instance = self.new(args)).instance_eval &block
      instance.raw
    end


    def self._where opt={}
      this = self
      results = self.call_api :get, "", opt

      begin
        out = { total: results['total'] }
        out[self::Resource.to_sym] = results[self::Resource].map { |r| this.new r }
        out
      rescue StandardError => e
        HoodTap.configuration.error HoodTap::Error.new("invalid result: #{ results }")
      end
    end

    def self._by_id id, opt={}
      self.create_instances self.call_api(:get, id, opt)
    end

    def self.call_api method, path, args={}
      if self::Methods.include? method
        HoodTap.call_api method, "#{ HoodTap.configuration.base_url }#{self::Resource }#{((path && path.length > 0) ? "/#{ path }.json" : '') }" , args
      end
    end

  end

end
