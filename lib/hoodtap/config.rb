module HoodTap

  class Config
    attr_accessor :base_url, :page_size, :key, :err, :defaults

    def url src
      @base_url = src
    end

    def page size
      @page_size = size
    end

    def api_key k
      @key = k
    end

    def on_error &block
      @on_err = block
    end

    def error *args
      @on_err.call *args
    end

    def api_defaults opt
      @defaults = opt
    end

    def to_s
      "<##{self.class.to_s}: #{ @base_url }|#{ @page_size }|#{ @key }|#{ @defaults }>"
    end

  end

  def self.configure &block
    @@_config ||= Config.new
    @@_config.instance_eval &block
    @@_config.api_defaults key: @@_config.key, page_size: @@_config.page_size
    @@_config
  end

  def self.configuration
    @@_config ||= Config.new
  end

end
