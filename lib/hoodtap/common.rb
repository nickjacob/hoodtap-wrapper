require 'httparty'

module HoodTap
  include HTTParty

  class Error < StandardError
  end

  protected

  def self.with_defaults args={}
    HoodTap.configuration.defaults ||= {}
    HoodTap.configuration.defaults.merge args
  end


  def self.call_api method, path, args={}
    args = self.with_defaults args
    case method
    when 'get', :get
      HoodTap.get path, :query => args
    else
      HoodTap.send method.to_sym, path, :body => args
    end
  end

  class API
    def self.request method, path, args={}
      HoodTap.call_api method, path, args
    end
  end


end
