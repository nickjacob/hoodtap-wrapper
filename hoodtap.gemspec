Gem::Specification.new do |s|
  s.name = 'hoodtap'
  s.version = '0.0.1'
  s.date = '2013-02-01'
  s.summary = 'an orm-like wrapper for the HoodTap API'
  s.authors = ['Nick Jacob']
  s.files = [ 'lib/hoodtap.rb', 'lib/hoodtap/common.rb', 'lib/hoodtap/config.rb', 'lib/hoodtap/resource.rb', 'lib/hoodtap/spec.rb' ]
end
